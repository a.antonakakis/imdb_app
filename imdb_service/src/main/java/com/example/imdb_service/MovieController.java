package com.example.imdb_service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/imdb")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MovieController {
    @Autowired
    private MovieService movieService;

    /**
     * This method will return a movie by its title. It will first check the database, and if the movie is not found, it will call the IMDB API.
     * @param title The title of the movie to search for.
     * @return The movie object or null if not found.
     * @see MovieService#getMovieByTitle(String)
     */
    @GetMapping(value = "/title/find")
    public Movie getMovie(@RequestParam(value = "title") String title) {
        return movieService.getMovieByTitle(title);
    }

    /**
     * This method will return a list of movies that have been searched for.
     * @return A list of movies.
     * @see MovieService#getMovieHistory()
     */
    @GetMapping(value = "/title/find/history")
    public Iterable<Movie> getMovieHistory() {
        return movieService.getMovieHistory();
    }

}
