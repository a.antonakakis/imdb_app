package com.example.imdb_service;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true, length = 255)
    private String title;
    @Column(name = "year", nullable = false)
    private int year;
    @Column(name = "running_time_in_minutes", nullable = false)
    private int runningTimeInMinutes;
    @Column(name = "lead_actor", nullable = false, length = 255)
    private String leadActor;
}
