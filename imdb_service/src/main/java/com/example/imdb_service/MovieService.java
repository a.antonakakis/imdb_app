package com.example.imdb_service;

import jakarta.transaction.Transactional;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.logging.Logger;

@Service
@Transactional
public class MovieService {
        @Autowired
        private MovieRepository movieRepository;

    /**
     * This method will return a movie by its title. It will first check the database, and if the movie is not found, it will call the IMDB API.
     * @param title The title of the movie to search for.
     * @return The movie object or null if not found.
     */
    public Movie getMovieByTitle(String title) {
            String lowercase_title = title.toLowerCase();

            Movie movie = movieRepository.findByTitle(lowercase_title).orElse(null);

            if (movie == null) {
                Logger.getLogger("MovieService").info("Movie '" + title + "' not found in database, calling IMDB API");

                String url = "https://imdb8.p.rapidapi.com/title/find?q=" + title;
                String apiKey = "e8c8d3f9dcmsh9a7d641252c910ep106a4fjsnecabc5bdd95d";

                WebClient client = WebClient.create(url);
                ResponseEntity < String > response = client.method(HttpMethod.GET).header("x-rapidapi-key", apiKey).header("x-rapidapi-host", "imdb8.p.rapidapi.com").retrieve().toEntity(String.class).block();

                if (response == null) {
                    Logger.getLogger("MovieService").warning("Failed to create ResponseEntity for IMDB API");
                    return null;
                }

                if (response.getStatusCode() != HttpStatusCode.valueOf(200)) {
                    Logger.getLogger("MovieService").warning("Connection to IMDB API failed");
                    return null;
                }

                if (response.getBody() == null || response.getBody().isEmpty() || !response.hasBody()) {
                    Logger.getLogger("MovieService").warning("Failed to get body from IMDB API");
                    return null;
                }

                try {
                    JSONObject json = new JSONObject(response.getBody());
                    JSONArray results = json.getJSONArray("results");
                    JSONObject movieJson = results.getJSONObject(0);

                    movie = new Movie();

                    movie.setTitle(movieJson.getString("title"));
                    movie.setYear(movieJson.getInt("year"));
                    movie.setRunningTimeInMinutes(movieJson.getInt("runningTimeInMinutes"));

                    String leadActor = movieJson.getJSONArray("principals").getJSONObject(0).getString("name");
                    movie.setLeadActor(leadActor);

                    movieRepository.save(movie);
                } catch (Exception e) {
                    Logger.getLogger("MovieService").info("Movie not found in IMDB API");
                    return null;
                }
            }
            else {
                Logger.getLogger("MovieService").info("Movie '" + title + "' found in database");
            }

            return movie;
        }

    /**
     * Get all movies from database
     * @return Iterable<Movie> a list of all movies in the database
     * @see Movie Movie class
     */
    public Iterable<Movie> getMovieHistory() {
            Logger.getLogger("MovieService").info("Getting movie history from database");
            return movieRepository.findAll();
        }

}
