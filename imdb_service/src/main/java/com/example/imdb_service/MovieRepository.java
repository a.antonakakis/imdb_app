package com.example.imdb_service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    // This is a custom query that will be used to find a movie by its title.
    // Since we don't know if the title is in upper or lower case, we use the lower().
    // There is no need to sanitize the input, since we are using a parameterized query.
    @Query("SELECT m FROM Movie m WHERE lower(m.title) = lower(:mtitle)")
    Optional<Movie> findByTitle(@Param("mtitle") String title);
}
