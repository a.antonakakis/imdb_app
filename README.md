# imdb_app

## Description
This is a simple app that uses the IMDB API to search for movies and display their details.

It is also saving the search history in a local database.

## Installation
1. Update the `application.properties` file with your database credentials.
2. Run the `mvn clean install` command to build the project.
3. Run the `mvn spring-boot:run` command to run the project.

## Usage
1. Use postman or any other tool to send a GET request to the following endpoint: `http://localhost:8080/api/imdb/title/find?title=interstellar`
2. The response will be a JSON object containing the movie details.
3. You can also use the following endpoint to get the search history: `http://localhost:8080/api/imdb/title/find/history`